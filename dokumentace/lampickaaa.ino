#include <OneWire.h> 
#include <DallasTemperature.h>

#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>






const char* ssid = "508";
const char* password = "Ladajekkt";

AsyncWebServer server(80);
#define ONE_WIRE_BUS D7

OneWire oneWire(ONE_WIRE_BUS); 

DallasTemperature sensors(&oneWire);


const char index_html[] PROGMEM = R"rawliteral(
  <!DOCTYPE HTML>
  <meta charset='utf-8'>
  <style>body{text-align: center;}div{border-style: solid;}</style>
  <title>Řídící panel</title>
  Lampička JAyJay
  <html><head></head><body><div><h1>%s</h1><p>teplota:%teplota% </p>
  <p><a href='/turn_on'>Zapnout</a></p>
  <p><a href='/duha'>DUHA</a></p></div>
  <p><a href='/vsechnybarvy'>Vsechny barvy</a></p></div>  
  </body></html>
  )rawliteral";

int red = D3;
int green = D4;
int blue = D7;
bool buttonState = 0;



void setup(void) { 

  Serial.begin(115200);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());




  
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(blue, OUTPUT);
  Serial.begin(9600); 
  sensors.begin(); 

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send_P(200, "text/html", index_html, processor);
  });

  server.on("/turn_on", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send_P(200, "text/html", index_html, processor);
    if(buttonState){
    analogWrite(red, 255);
    analogWrite(green, 255);
    analogWrite(blue, 255);
    buttonState = 1;
  }else{
    analogWrite(red, 0);
    analogWrite(green, 0);
    analogWrite(blue, 0);
    buttonState = 0;
  }
  });
 
  server.on("/vsechnybarvy", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send_P(200, "text/html", index_html, processor);
    analogWrite(red, 255);
    analogWrite(green, 0);
    analogWrite(blue, 0);
    delay(500);
    analogWrite(red, 0);
    analogWrite(green, 0);
    analogWrite(blue, 255);
    delay(500);
    analogWrite(red, 0);
    analogWrite(green, 255);
    analogWrite(blue, 0);
  });
 


  AsyncElegantOTA.begin(&server);
  // Start server
  server.begin();

}



String processor(const String& var){
  Serial.println(var);
  if(var == "teplota"){
      return String(sensors.getTempCByIndex(0));
  }
  return String();
}


void loop(void) { 
  sensors.requestTemperatures();
  Serial.print(sensors.getTempCByIndex(0));
  delay(1000); 
} 
